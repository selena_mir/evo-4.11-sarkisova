﻿using UnityEngine;
using System.Collections;

public class MovingDot : MonoBehaviour
{

    float speed = 5f;
    float angle = Random.Range(-3.14f, 3.14f);
    int delay = 0;

    void Update()
    {
        float deltaX = Mathf.Cos(angle) * speed * Time.deltaTime;
        float deltaY = Mathf.Sin(angle) * speed * Time.deltaTime;
		float deltaZ = Mathf.Tg(angle) * speed * Time.deltaTime;


        if ((transform.position.x <= -Game.Instance.fieldSize.x / 2) || (transform.position.x >= Game.Instance.fieldSize.x / 2))
            if (delay == 0)
            {
                angle = (angle > 0 ? 1 : -1) * 3.14f - angle;
                delay = 3;
            }
            else
                delay--;
        if ((transform.position.y <= - Game.Instance.fieldSize.y / 2) || (transform.position.y >= Game.Instance.fieldSize.y / 2))
            if (delay == 0)
            {
                delay = 3;
				angle *= -1;
            }
            else
                delay--;
    }
}